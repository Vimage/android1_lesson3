# README #

## Задание ##

* Разобраться с элементами UI: LargeText, MediumText, SmallText; CheckBox, RadioButton; Элементами категории «Text Fields»; Элементами категории «Layouts»;

* Придумать дизайн приложения AboutMe;

* Создать разметку экрана для приложения AboutMe (портретная и альбомная ориентации);